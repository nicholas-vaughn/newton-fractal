import numpy as np
from PIL import Image

# Returns a RGBA color in 32 bit format
def rgb(r, g, b, a=255):
    return (a << 24) + (b << 16) + (g << 8) + r

# Map of an index to a color
color_map = {0: rgb(200, 140, 30),
             1: rgb(120, 150, 30),
             2: rgb(20, 80, 100),
             3: rgb(150, 70, 70),
             4: rgb(80, 10, 30),
             5: rgb(30, 10, 80)}

# vectorized function to map an array of indexs to an array of colors
color_map_fn = np.vectorize(color_map.__getitem__, otypes=[np.uint32])

def generate_fractal(poly, rect, grid_shape, delta=0.1):
    """Generates a Newton's fractal of the given polynomial within the given rectangle

    Args:
        poly (numpy.poly1d): the polynomial to use
        rect (tuple): a tuple in form of (left, top, width, height) representing the
        complex coordinates to generate the fractal in. Top and height are assumed to 
        be imaginary.
        grid_shape (tuple): a tuple in form of (width, height) representing the
        size of the output fractal grid
        delta (float, optional): The minimum update magnitude used to define convergence.
        Defaults to 0.1.

    Returns:
        array: Returns an array with shape grid_shape where each value is the index of
        the root of the polynomial that the point on the grid converged to
    """
    dpoly = poly.deriv()
    left, top, width, height = rect
    grid_width, grid_height = grid_shape

    real = np.linspace(left, left + width, grid_width)
    # generates imaginary components, top + height first so grid has the same
    # direction as the complex plane
    imag = np.linspace(complex(0, top + height), complex(0, top), grid_height)
    # generates grid of numbers in the complex plane
    vals = np.add.outer(imag, real)

    not_converged = np.ones_like(vals, dtype=bool)
    while not_converged.any():
        # performs one iteration of Newton's method on the points that have not
        # converged
        vals_to_update = vals[not_converged]
        update = poly(vals_to_update) / dpoly(vals_to_update)
        vals[not_converged] -= update
        not_converged[not_converged] = abs(update) > delta
    
    return np.argmin(abs(np.subtract.outer(poly.r, vals)), 0)

def generate_image(fractal, size):
    """Returns an image of the given fractal

    Args:
        fractal (array): fractal to generate image of 
        size (tuple): dimension of output image

    Returns:
        PIL.Image: an image of the fractal with specified size
    """
    return Image.fromarray(color_map_fn(fractal), mode='RGBA').resize(size)

poly = np.poly1d([2, -2, 3, -1, 2, -1, 5])
fractal = generate_fractal(poly, (-1, -1, 2, 2), (3000, 3000))
generate_image(fractal, (1000, 1000)).show()